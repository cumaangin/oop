<?php
require_once 'animal.php';
class Ape extends Animal{
    public $yell;
    public function __construct($name,$legs,$cold_blooded,$yell){
        $this->name=$name;
        $this->legs=$legs;
        $this->cold_blooded=$cold_blooded;
        $this->yell=$yell;
    }
    public function get_yell(){
        return $this->yell;
    }
}
?>