<?php
require_once 'animal.php';
class Frog extends Animal{
    public $jump;
    public function __construct($name,$legs,$cold_blooded,$jump){
        $this->name=$name;
        $this->legs=$legs;
        $this->cold_blooded=$cold_blooded;
       $this->jump=$jump;
    }
    public function get_jump(){
        return $this->jump;
    }
}
?>