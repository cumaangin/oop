<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP PHP</title>
</head>
<body>
    <h2>SOAL 1 OOP PHP</h2>
<?php
    require_once 'animal.php';
    require_once 'Frog.php';
    require_once 'Ape.php';
$sheep = new Animal("shaun",4,"no");
//echo $sheep->name; // "shaun"
//echo $sheep->legs; // 4
//echo $sheep->cold_blooded;
//$sheep->set_name("heli");
echo "Name : ".$sheep->get_name();
//$sheep->set_legs(2);
echo "<br>";
echo "Legs : ".$sheep->get_legs();
echo "<br>";
//$sheep->set_cold_blooded("yes");
echo "Cold Blooded : ".$sheep->get_cold_blooded();
//echo "test php";
echo "<br>";
?>
    <h2>SOAL 2 OOP PHP</h2>
<?php
/*   RELEASE 1   */
/*   KODOK   */
$kodok = new Frog("buduk",4,"no","Hop Hop");
echo "Name : ".$kodok->get_name();echo "<br>";
echo "Legs : ".$kodok->get_legs();echo "<br>";
echo "Cold Blooded: ".$kodok->get_cold_blooded();echo "<br>";
echo "Jump : ".$kodok->get_jump();echo "<br><br>";
/*     APE     */
$sungokong = new Ape("kera sakti",2,"no","Auwoooo");
echo "Name : ".$sungokong->get_name();echo "<br>";
echo "Legs : ".$sungokong->get_legs();echo "<br>";
echo "Cold Blooded : ".$sungokong->get_cold_blooded();echo "<br>";
echo "Yell : ".$sungokong->get_yell();echo "<br><br>";
?>
</body>
</html>
